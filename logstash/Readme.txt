Logstash container
Like before, create following folders for logstash and its configuration settings in the root of 
the project.

config : The config folder will hold logstash system wide configuration settings.

pipeline : The pipeline folder will hold logstash configuration settings for each log file that you 
           want to process.

logfile : The logfile folder will contain the log file (network logs, Apache logs and so on)
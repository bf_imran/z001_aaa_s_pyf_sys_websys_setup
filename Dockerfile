FROM phusion/passenger-full:1.0.9

ENV HOME /root

CMD ["/sbin/my_init"]

RUN apt-get update 
RUN apt-get install sudo
RUN apt-get install -y curl
RUN sudo apt install -y git
RUN sudo apt-get install python3
RUN sudo apt install -y python3-pip
RUN sudo pip3 install flask
RUN sudo pip3 install Flask-MySQLdb
RUN rm -f /etc/service/nginx/down
RUN rm /etc/nginx/sites-enabled/default

COPY ./webapp.conf /etc/nginx/sites-enabled/webapp.conf

RUN mkdir /home/app/webapp
COPY --chown=app:app ./code /home/app/webapp

WORKDIR /home/app/webapp
RUN sudo pip3 install -r requirements.txt

RUN pip3 install -e .

RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
